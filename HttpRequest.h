#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <iostream>
#include <functional>

#include <string>
#include <cstddef>
#include <vector>

#include<ctime>
#include<time.h>

//#include "HttpResponse.h"


using namespace std;
#define backlog 20
#define _BSD_SOURCE
#define buffsize 280000


class HttpRequest {
 private:
  int UID;
  string TIME;
  string REQUEST;
  string IPFROM;

  int maxAge;
  int maxStale;
  int minFresh;
  int noCache;
  int noStore;

  vector<char> request;

  //Initialize the UID field given a request.
  void initUID(vector<char> req) {
    UID = req.size();
    //cout << "UID is: " << UID << endl;
  }
  

  //Initialize the UTC time when the request was received by the proxy.
  void initTIME() {
    time_t currTime = time(0);
    struct tm* nowTime = gmtime(&currTime);
    const char* t = asctime(nowTime);
    TIME = string(t);
    //cout << "UTC time is now:" << TIME << endl;
  }


  //Initialize the IPFROM field.
  void initIPFROM(vector<char> req) {
    IPFROM = "to be implemented";
    //cout << IPFROM << endl;
  }
  

  //Initialize the REQUEST field.
  void initREQUEST(vector<char> req) {
    string temp(req.begin(), req.end());
    if (temp.find("GET") == string::npos) {
      REQUEST = temp.substr(0, temp.find("\r\n"));
    } else {
      temp = temp.substr(temp.find("GET"), temp.length());
      temp = temp.substr(0, temp.find("\r\n"));
      REQUEST = temp;
      //cout << "The REQUEST field is:" << REQUEST << endl;
    }
    //cout << "Finished initREQUEST" << endl;;
  }

  int toInteger(string s) {
    int ans = 0;
    int i = 0;
    while(1){
      if (i == s.length() || s[i] == '\0' || s[i] > '9' || s[i] < '0') break;
      ans = ans * 10 + s[i] - '0';
      i++;
    }
    return ans;
  }
  

 public:
  //A constructor that takes a vector<char> which contains a
  //request received from a client. 
  HttpRequest(vector<char> req) {
    initUID(req);
    initTIME();
    initIPFROM(req);
    initREQUEST(req);
    for (int i = 0; i < req.size(); i++) request.push_back(req[i]);

    //Get max-age directive of the cache control field.
    //Case 1: The request does not have a cache control header field.
    string temp(req.begin(), req.end());
    if (temp.find("Cache-Control:") == string::npos) {
      maxAge = -1;
      maxStale = -1;
      minFresh = -1;
      noCache = 0;
      noStore = 0;
    } else {
      //Case 2: The request does have a cache control field.
      //Initialize maxAge. 
      if (temp.find("max-age=") != string::npos) {
	maxAge = toInteger(temp.substr(temp.find("max-age=") + 8, temp.length()));
      } else {
	maxAge = -1;
      }
      cout << "max-age is " << maxAge << endl;

      //Initialize maxStale
      if (temp.find("max-stale=") != string::npos) {
	maxStale = toInteger(temp.substr(temp.find("max-stale=") + 10, temp.length()));
      } else {
	maxStale = -1;
      }
      cout << "max-stale is " << maxStale << endl;
      
      //Initialize minFresh
      if (temp.find("min-fresh=") != string::npos) {
	minFresh = toInteger(temp.substr(temp.find("min-fresh=") + 10, temp.length()));
      } else {
	minFresh = -1;
      }
      
      //Initialize noCahe. 
      if (temp.find("no-cache") != string::npos) {
	noCache = 1;
      } else {
	noCache = 0;
      }
      
      //Initialize noCahe. 
      if (temp.find("no-store") != string::npos) {
	noStore = 1;
      } else {
	noStore = 0;
      }           
    }
  }


  bool isGET() {
    string temp(request.begin(), request.end());
    if (temp.find("GET") == string:: npos) return false;
    return true;
  }

  
  int getUID() { return UID;}
  string getTIME(){ return TIME;}
  string getIPFROM(){ return IPFROM;}
  string getREQUEST() { return REQUEST;}
  int getMaxAge() { return maxAge;}
  int getMaxStale() { return maxStale; }
  int getMinFresh() { return minFresh; }
  int getNoCache() { return noCache; }
  int getNoStore() { return noStore; }
};
